// typeof在判断null和Date时不太准确，可以通过使用Object.prototype.toString实现
function typeOf(obj) {
    // console.log(Object.prototype.toString.call(obj)) #[object Array]
    let res = Object.prototype.toString.call(obj).split(' ')[1]
    // console.log(res.substring(0,res.length-1)) #Array
    res = res.substring(0,res.length-1).toLowerCase()
    return res
}
console.log(typeOf([]))  //array
console.log(typeOf({}))  //object
