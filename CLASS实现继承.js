//父类
class People {
  //类的传参
  constructor(name) {
    this.name = name;
  }
  // 类中都是方法函数
  eat() {
    console.log(`${this.name} eat something`);
  }
}

//子类 继承父类 里面的方法可以直接调用，属性需要调用super去执行
class Student extends People {
  constructor(name, number) {
    //调用super,People会自动帮我们处理name
    super(name);
    //自己处理，学生才有自己的学号
    this.number = number;
  }
  sayHi() {
    console.log(`姓名 ${this.name} 学号 ${this.number}`);
  }
}

//子类
class Teacher extends People {
  constructor(name, major) {
    super(name);
    this.major = major;
  }
  teach() {
    console.log(`${this.name} 教授 ${this.major}`);
  }
}

//实例
const xialuo = new Student("夏洛", 100);
console.log(xialuo.name);
console.log(xialuo.number);
xialuo.sayHi();
xialuo.eat();

//实例
const wanglaoshi = new Teacher("王老师", "语文");
console.log(wanglaoshi.name);
console.log(wanglaoshi.major);
wanglaoshi.teach();
wanglaoshi.eat();
