// ES5实现数组去重
function unique(arr) {
  var res = arr.filter(function (item, index, array) {
    return array.indexOf(item) === index;
  });
  return res;
}
// ES6实现数组去重
var unique = (arr) => [...new Set(arr)];
let arr = [1, 2, 3, 4, 2, 5, 6, 4, 5];
console.log(unique(arr)); // (6) [1, 2, 3, 4, 5, 6]
