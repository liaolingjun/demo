// 防抖：防抖就是指触发事件后在 n 秒内函数只能执行一次。
// 如果在 n 秒内又触发了事件，则会重新计算函数执行时间。
// 举例：外卖员接配送订单(将多次操作转化成一次)
//  接到一个配送单后，心想在等三分钟，如果三分钟没有配送订单就去配送。
// 如果又有一个配送订单，就会想着再等三分钟，直到三分钟没有什么订单，就开始配送。
// 参数是执行函数和等待时间
function debounce(fn, delay) {
  let timerId = null;
  return function () {
    // 如果接到订单就再等3分钟
    if (timerId) {
      window.clearTimeout(timerId);
    }
    // 3分钟没有接到订单就直接配送
    timerId = setTimeout(() => {
      fn.apply(this, arguments);
      timerId = null;
    }, delay);
  };
}

// 节流：节流就是指连续触发事件但是在 n 秒中只执行一次函数。
// 节流会稀释函数的执行频率（n秒内只执行一次操作）
function torottle(fn, delay) {
  // 设置一个触发开关
  let canUse = true;
  // 如果为true，就触发技能，否则就不触发
  return function () {
    if (canUse) {
      fn.apply(this, arguments);
      // 触发后，关闭开关
      canUse = false;
      // 在3秒后打开开关
      setTimeout(() => (canUse = true), delay);
    }
  };
}
