// 数组扁平化就是将 [1, [2, [3]]] 这种多层的数组
// 拍平成一层 [1, 2, 3]。
// ES5实现，递归。
function flatten(arr) {
    var result = [];
    for (var i =0,len = arr.length; i< len; i++) {
        // 调用判断数组是否为数组的方法。
        if (Array.isArray(arr[i])) {
            result = result.concat(flatten(arr[i]))
        } else {
            result.push(arr[i])
        }
    }
    return result
}

let arr = [[1,2],3,4,[2,5],6,4,5]
console.log(flatten(arr)) //  [1, 2, 3, 4, 2, 5, 6, 4, 5]

//ES6实现
function flatten(arr) {
    while (arr.some(item => Array.isArray(item))) {
        arr = [].concat(...arr);
    }
    return arr;
}
